function defaults(obj, defaultProps) {
    if( !obj) {
        return defaultProps ;
    }
    else {
        for (const key in defaultProps) {
            if(!(key in obj)) {
                obj[key] = defaultProps[key]
            }
        }
        return obj ;
    }
}
module.exports = defaults ;