const values = (obj) => {
    if(!obj) {
        return []
    }
    else {
        let valuesArray = []
        for (const key in obj) {
            typeof(obj[key]) === 'function' ? "": valuesArray.push(obj[key])
        }
        return valuesArray
    }
}

module.exports = values ;