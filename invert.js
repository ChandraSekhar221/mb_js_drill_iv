function invert(obj) {
    if (!obj) return {} // If obj has no values then empty object will be returned
    else {
        let myObj = {} // Empty Object created 
        for (const key in obj) {
            myObj[obj[key]] = key
        }
        return myObj // Returning the resultant Obj after loop completeion
    }
}

module.exports = invert ;