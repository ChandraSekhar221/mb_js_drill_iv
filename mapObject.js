function mapObject(obj,cb) {
    if (!obj || !cb) return {}
    else {
        let myObj = {}
        for (let key in obj) {
            let newValue = cb(obj[key],key,obj)
            myObj[key] = newValue
        }
        return myObj ;
    }
}
module.exports = mapObject ;