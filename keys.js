function findKeys(testObject) {
    if (!testObject) {
        return [];
    }
    else {
        let keyArray = [];
        for (let key in testObject) {
            keyArray.push(key);
        }
        return keyArray;
    }
}

module.exports = findKeys ;